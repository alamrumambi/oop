<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    require './animal.php';
    require './ape.php';
    require './frog.php';

    $sheep = new Animal("shaun");
    echo "Name : " . $sheep->get_name() . "<br>"; // "shaun"
    echo "Legs : " . $sheep->get_legs() . "<br>"; // 4
    echo "Cold Blooded : " . $sheep->get_cold_blooded() . "<br>"; // "no"
    echo "<br>";
    $kodok = new Frog("buduk");
    echo "Name : " . $kodok->get_name() . "<br>";
    echo "Legs : " . $kodok->get_legs() . "<br>";
    echo "Cold Blooded : " . $kodok->get_cold_blooded() . "<br>";
    echo "Jump : ";
    $kodok->jump(); // "hop hop"
    echo "<br><br>";
    $sungokong = new Ape("kera sakti");
    echo "Name : " . $sungokong->get_name() . "<br>";
    echo "Legs : " . $sungokong->get_legs() . "<br>";
    echo "Cold Blooded : " . $sungokong->get_cold_blooded() . "<br>";
    echo "Yell : ";
    $sungokong->yell(); // "Auooo"
    ?>
</body>

</html>